<?php

namespace Piki\Core\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CoreController extends Controller
{

    public function index(Request $request)
    {
        return 'index';
    }

    public function aboutUs(Request $request)
    {
        return 'about us';
    }

    public function services(Request $request)
    {
        return 'services';
    }

    public function commerce(Request $request)
    {
        return 'commerce';
    }

}
