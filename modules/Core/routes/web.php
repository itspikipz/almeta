<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'CoreController@index',
    'as' => 'index'
]);

Route::get('/about-us', [
    'uses' => 'CoreController@aboutUs',
    'as' => 'about-us'
]);

Route::get('/services', [
    'uses' => 'CoreController@services',
    'as' => 'services'
]);

Route::get('/commerce', [
    'uses' => 'CoreController@commerce',
    'as' => 'commerce'
]);
